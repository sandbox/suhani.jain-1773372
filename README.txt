-- SUMMARY --

This is a simple module that provides a block and/or a shortcut key(Ctrl+m) for
searching and jumping to selected menu. 


-- REQUIREMENTS --

This module requires shortcut.js, a javascript library. This has been
downloaded and placed into search_menu module folder. The location is
http://www.openjs.com/scripts/events/keyboard_shortcuts/shortcut.js.


-- INSTALLATION --

Follow the installation guide here - http://drupal.org/node/70151 to install
this module.
