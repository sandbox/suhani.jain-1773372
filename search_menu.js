jQuery(document).ready(function ($) {
  var popupVisible = false;

  shortcut.add("Ctrl+m",function() {
    $("#search-head").css('display', 'block');
    $("#search_form_contents").css('display', 'block');
    showSearchMenu();
    return false;
  },{
  });

  jQuery('input:#edit-menu-text--2').keyup(function (e) {
    if (e.which == 13) {
      var value = $('#edit-menu-text--2').val();
      if ($('#edit-menu-text--2').val() != '' && ~value.indexOf('/')) {
        var element = $("#edit-menu-text--2").val().split("#");
        $("#edit-menu-text--2").val(element[1]);
        window.location = (element[0]);
        $('#edit-menu-text').val('');
      }
    }
  });
  jQuery('input:#edit-menu-text--2').focusout(function (e) {
    var value = $('#edit-menu-text--2').val();
    if ($('#edit-menu-text--2').val() != '' && ~value.indexOf('/')) {
      if (e.which != 13) {
        var element = $("#edit-menu-text--2").val().split("#");
        $("#edit-menu-text--2").val(element[1]);
        window.location = (element[0]);
        $('#edit-menu-text--2').val('');
      }
    }
  });

  jQuery('input:#edit-menu-text').mouseover(function () {
    $("#edit-menu-text").attr("title","Enter the menu name you wish to search for.");
  });

  jQuery('input:#edit-menu-text--2').mouseover(function () {
    $("#edit-menu-text--2").attr("title","Enter the menu name you wish to search for.");
  });

  jQuery('input:#edit-menu-text').keyup(function (e) {
    if (e.which == 13) {
      var value = $('#edit-menu-text').val();
      if ($('#edit-menu-text').val() != '' && ~value.indexOf('/')) {
        var element = $("#edit-menu-text").val().split("#");
        $("#edit-menu-text").val(element[1]);
        window.location = (element[0]);
      }
    }
  });
  jQuery('input:#edit-menu-text').focusout(function (e) {
    var value = $('#edit-menu-text').val();
    if ($('#edit-menu-text').val() != '' && ~value.indexOf('/')) {
      if (e.which != 13) {
        var element = $("#edit-menu-text").val().split("#");
        $("#edit-menu-text").val(element[1]);
        window.location = (element[0]);
        $('#edit-menu-text').val('');
      }
    }
  });

  function showSearchMenu() {
    var settings = Drupal.settings.searchMenu;
    var searchBox = $("#search_menu_box");
    if (!popupVisible) {
      popupVisible = true;
      if(settings.hideObjects) {
        $("object, embed").css("visibility", "hidden");
      }
      $("#search_dim_screen").css({
        "position" : "fixed",
        "top" : "0",
        "left" : "0",
        "height" : "100%",
        "width" : "100%",
        "display" : "block",
        "background-color" : settings.screenFadeColor,
        "z-index" : settings.screenFadeZIndex,
        "opacity" : "0"
      }).fadeTo(settings.dimFadeSpeed, 0.8, function() {
        searchBox.css({
          "position" : "fixed",
          "padding" : "10px",
          "width" : settings.searchBoxWidth,
          "height" : settings.searchBoxHeight
        });
        var wHeight = window.innerHeight ? window.innerHeight : $(window).height();
        var wWidth = $(window).width();
        var eHeight = searchBox.height();
        var eWidth = searchBox.width();
        var eTop = (wHeight - eHeight) / 2;
        var eLeft = (wWidth - eWidth) / 2;
        if ($("#search_close_button").css("display") === "none") {
          $("#search_close_button").css("display", "inline");
        }
        searchBox.css({
          "top" : eTop,
          "left" : eLeft,
          "color" : settings.searchBoxTextColor,
          "background-color" : settings.searchBoxBackgroundColor,
          "border-style" : settings.searchBoxBorderStyle,
          "border-color" : settings.searchBoxBorderColor,
          "border-width" : settings.searchBoxBorderWidth,
          "z-index" : (settings.screenFadeZIndex + 1),
          "display" : "none"
        })
        .fadeIn(settings.boxFadeSpeed);
        searchBox.find(".form-text:first").focus().select();
        $("#search_close_button").css('right','10');
        setCloseListener();
      });
    }
  }

  function setCloseListener() {
    $("#search_close_button").click(function() {
      hideSearchMenu();
      return false;
    });
    $(window).keyup(function (e) {
      if (e.which == 27) {
        hideSearchMenu();
        return false;
      }
    });
    jQuery('input:#edit-menu-text--2').keyup(function (e) {
      if (e.which == 27) {
        hideSearchMenu();
        return false;
      }
    });
  }

  function hideSearchMenu() {
    var settings = Drupal.settings.searchMenu;
    if (popupVisible) {
      popupVisible = false;
      $("#search_menu_box").fadeOut(settings.boxFadeSpeed, function() {
        $(this).css({
          "position" : "static",
          "height" : "auto",
          "width" : "auto",
          "background-color" : "transparent",
          "border" : "none"
        });
        $("#search_dim_screen").fadeOut(settings.dimFadeSpeed, function() {
          if(settings.hideObjects) {
            $("object, embed").css("visibility", "visible");
          }
        });
        $(window).focus();
      });
    }
  }
});
